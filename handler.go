package main

import (
	"context"
	"log"

	"github.com/micro/go-micro/metadata"
	pb "gitlab.com/lazybasterds/alpaca/node-service/proto/node"
)

type service struct {
	db *DBConnection
}

func (s *service) GetRepo() Repository {
	return &NodeRepository{s.db}
}

func (s *service) GetNodesPerFloor(ctx context.Context, req *pb.Node, res *pb.NodeResponse) error {
	log.Println(ctx)
	repo := s.GetRepo()
	defer repo.Close(ctx)

	mdata, ok := metadata.FromContext(ctx)
	var requestID string
	if ok {
		requestID = mdata["requestid"]
	}
	floorID := req.FloorID
	nodeType := req.Type
	sortBy := req.SortBy
	nodes, err := repo.GetNodesPerFloor(ctx, floorID, nodeType, sortBy)
	if err != nil {
		log.Printf("Error: Failed to get nodes of type %s for floor with id %s : %+v", nodeType, floorID, err)
		return err
	}
	log.Printf("Method: GetNodesPerFloor, RequestID: %s, Return: %+v", requestID, nodes)
	res.Nodes = nodes
	return nil
}

func (s *service) GetAllNodes(ctx context.Context, req *pb.Node, res *pb.NodeResponse) error {
	log.Println(ctx)
	repo := s.GetRepo()
	defer repo.Close(ctx)

	mdata, ok := metadata.FromContext(ctx)
	var requestID string
	if ok {
		requestID = mdata["requestid"]
	}
	nodeType := req.Type
	sortBy := req.SortBy
	nodes, err := repo.GetAllNodes(ctx, nodeType, sortBy)
	if err != nil {
		log.Printf("Error: Failed to get all nodes : %+v", err)
		return err
	}
	log.Printf("Method: GetAllNodes, RequestID: %s, Return: %+v", requestID, nodes)
	res.Nodes = nodes
	return nil
}

func (s *service) GetNodeByID(ctx context.Context, req *pb.Node, res *pb.NodeResponse) error {
	log.Println(ctx)
	repo := s.GetRepo()
	defer repo.Close(ctx)

	mdata, ok := metadata.FromContext(ctx)
	var requestID string
	if ok {
		requestID = mdata["requestid"]
	}

	nodeID := req.Id
	node, err := repo.GetNodeByID(ctx, nodeID)
	if err != nil {
		log.Printf("Error: Failed to get node with ID %s : %+v", nodeID, err)
		return err
	}
	log.Printf("Method: GetNodeByID, RequestID: %s, Return: %+v", requestID, node)
	res.Node = node
	return nil
}
